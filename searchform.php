<form action="<?php echo site_url(); ?>" method="get">
    
    <div class="input-group mb-3">
    <input type="text" name="s" class="form-control" placeholder="search " aria-label="search the site" aria-describedby="search the site" value="<?php the_search_query(); ?>">
    <div class="input-group-append">
        <button class="btn btn-primary" type="button"> <i class="fas fa-search"></i> </button>
    </div>
    </div>
</form>