<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Affiliate_Program_ET
 */

?>
	<div class="bg-white border-top border-light pt-4 pb-3">
		<footer id="colophon" class="site-footer container">
			<div class="row">
				<div class="col-lg-4 col-12 mb-4 ">
					<h4 class="text-primary" > <?php bloginfo('name'); ?></h4>
					<div class="text-dark " > <?php the_field('footer_description', 'option') ?> </div>
					<?php if(have_rows('social_media', 'option')): ?>
						<?php while(have_rows('social_media', 'option')): the_row();
							$title = get_sub_field('title_of_the_link');
							$icon_name = get_sub_field('icon_name');
							$link = get_sub_field('link');
							?>
							<a href="<?php echo $link; ?>" class="text-decoration-none text-primary" title="<?php echo $title; ?>" > 
								<i class="fab fa-<?php echo $icon_name;?> fs-25 mr-2"></i>
							</a>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div class="col-lg-8 col-12 mb-4">
					<?php get_template_part( 'footer', 'widgets' ); ?>
				</div>
			</div>


			<div class="site-info text-center border-top pt-3 border-light d-flex flex-row justify-content-between">
				<span class="text-left">
				© 2020 <?php bloginfo('name') ?>
				</span>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
