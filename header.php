<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Affiliate_Program_ET
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class('pt-5 '); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site" >
	<a class="skip-link screen-reader-text d-none" href="#primary"><?php esc_html_e( 'Skip to content', 'affiliate-program-et' ); ?></a>

	<header id="masthead" class="site-header site-header navbar-static-top navbar-light bg-white fixed-top shadow-sm">
		<div class="site-branding d-none">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$affiliate_program_et_description = get_bloginfo( 'description', 'display' );
			if ( $affiliate_program_et_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $affiliate_program_et_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->
		<div class="container">

		<nav id="site-navigation" class="main-navigation navbar navbar-expand-lg navbar-light bg-white ">
		<div class="navbar-brand">
                
				<?php
				if (has_custom_logo()):
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' ); ?>
					<a href="<?php echo esc_url(home_url('/')); ?>">
						<?php   echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '" class="w-100%" style="max-height:60px;" >'; ?>
					</a>
				<?php else: ?>
					<a class="site-title" href="<?php echo esc_url(home_url('/')); ?>"><?php esc_url(bloginfo('name'));?></a>
				<?php endif;?>

			</div>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end">
			
			<?php   
					wp_nav_menu(array(
					'theme_location'    => 'primary',
					'container'       => 'div',
					'container_id'    => 'main-nav',
					'container_class' => 'collapse navbar-collapse justify-content-end',
					'menu_id'         => false,
					'menu_class'      => 'navbar-nav text-capitalize',
					'depth'           => 3,
					'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
					'walker'          => new wp_bootstrap_navwalker()
					));
				?>

			</div>
		</nav><!-- #site-navigation -->
		</div>
	</header><!-- #masthead -->
