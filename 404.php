<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Affiliate_Program_ET
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found container py-4">
		
			<div class="page-content row">
				<div class="col-lg-8 col-12">
				<header class="page-header">
				<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'affiliate-program-et' ); ?></h1>
			</header><!-- .page-header -->

					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'affiliate-program-et' ); ?></p>

						<?php
						get_search_form();
						?>

				</div>
				<div class="col-lg-4 col-12 sidebar">
					<?php get_sidebar( ) ?>
				</div>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
