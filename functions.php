<?php
/**
 * Affiliate Program ET functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Affiliate_Program_ET
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'affiliate_program_et_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function affiliate_program_et_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Affiliate Program ET, use a find and replace
		 * to change 'affiliate-program-et' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'affiliate-program-et', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'affiliate-program-et' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'affiliate_program_et_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'affiliate_program_et_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function affiliate_program_et_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'affiliate_program_et_content_width', 640 );
}
add_action( 'after_setup_theme', 'affiliate_program_et_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function affiliate_program_et_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'affiliate-program-et' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'affiliate-program-et' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
	
		register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer 1', 'affiliate-program-et' ),
			'id'            => 'footer-1',
			'description'   => esc_html__( 'Add widgets here.', 'affiliate-program-et' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));

		register_sidebar( 
				array(
					'name'          => esc_html__( 'Footer 4', 'affiliate-program-et' ),
					'id'            => 'footer-4',
					'description'   => esc_html__( 'Add widgets here.', 'affiliate-program-et' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '<h2 class="widget-title">',
					'after_title'   => '</h2>',
				));

		register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer 2', 'affiliate-program-et' ),
			'id'            => 'footer-2',
			'description'   => esc_html__( 'Add widgets here.', 'affiliate-program-et' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
		
		register_sidebar( 
		array(
			'name'          => esc_html__( 'Footer 3', 'affiliate-program-et' ),
			'id'            => 'footer-3',
			'description'   => esc_html__( 'Add widgets here.', 'affiliate-program-et' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'affiliate_program_et_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function affiliate_program_et_scripts() {
	wp_enqueue_style( 'affiliate-program-et-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'affiliate-program-et-style', 'rtl', 'replace' );
	wp_enqueue_style( 'star-rating', get_template_directory_uri() .'/css/star-rating.css' , array() );
	wp_enqueue_script( 'jquery-js', 'https://code.jquery.com/jquery-3.5.1.min.js');
	wp_enqueue_script( 'start-rating-js', get_template_directory_uri().'/js/star-rating.js', array('jquery-js'),null, false );
	wp_register_script( 'mixitup', get_template_directory_uri().'/js/mixitup.min.js', array('jquery-js'),null, false );

	wp_enqueue_script( 'affiliate-program-et-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'affiliate_program_et_scripts' );

/**
 * Remove post-formats support from posts.
 */
function wpdocs_remove_page_type_support() {
    remove_post_type_support( 'page', 'editor' );
}
add_action( 'init', 'wpdocs_remove_page_type_support', 10 );

function wpdocs_excerpt_more( $more ) {
    return '<a href="'.get_the_permalink().'" rel="nofollow" class="ml-1" >Read More...</a>';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


/**
 * excerpt lenght
 */
function mytheme_custom_excerpt_length( $length ) {
    return 32;
}
add_filter( 'excerpt_length', 'mytheme_custom_excerpt_length', 999 );


add_image_size( 'post-thumb', 300, 220, true );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Function to include ACF
 */
require get_template_directory(). '/inc/include-acf.php';

/**
 * Function include CPT
 */
require get_template_directory(). '/inc/cpt/cpt.php';


/**
 * Taxonomies includes
 */
require get_template_directory(). '/inc/taxonomies/taxonomies.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/**
 * Get those option pages here
 * 
 */

get_template_part( 'inc/cpt/option', 'page' );
/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

if ( ! file_exists( get_template_directory() . '/wp-bootstrap-navwalker.php' ) ) {
	// file does not exist... return an error.
	return new WP_Error( 'wp-bootstrap-navwalker-missing', __( 'It appears the wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
  } else {
	// file exists... require it.
	require_once get_template_directory() . '/wp-bootstrap-navwalker.php';
  }