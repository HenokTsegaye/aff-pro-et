<?php

/**
 * Expected Varaiables
 * $first_button
 * $second_button;
 */
?>
<div class="bg-light pt-4">
    <div class="container py-5 ">
        <?php get_template_part('blocks/components/content', 'text'); ?>
        <div class="pt-4 pb-2">
            <?php if(!empty($first_button)): ?>
                <a href="<?php echo $first_button['link']['url'] ?>" 
                class="btn btn-primary border-0 btn-lg" > 
                <?php echo $first_button['title']; ?>
                </a> 
            <?php endif; ?>
            <?php if(!empty($second_button)): ?>
                <a href="<?php echo $second_button['link']['url'] ?>" 
                class="btn btn-light border border-muted btn-lg" > 
                <?php echo $second_button['title']; ?>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>