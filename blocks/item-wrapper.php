<?php
/**
 * Post Grid Render
 * 
 * expected Variables
 * $title
 * $content
 * $background_color
 * $text_color
 * $list_type
 */

$list_type = get_query_var( 'list_type', 'list' );
set_query_var( 'list_type', $list_type );
$background_color = get_query_var("background_color", false);
$text_color = get_query_var("text_color", false);

if(!wp_script_is("star-rating-js")) {
    wp_enqueue_script( 'start-rating-js');
    wp_enqueue_script( 'mixitup');
}

$terms = get_terms( array(
    'taxonomy' => 'item_type',
    'hide_empty' => false,
));

$unique_id = uniqid();
$text_color = false;
 ?>

<div style="<?php echo $background_color?"background-color:".$background_color. ";":null; echo $text_color?"color:".$text_color. ";":null; ?>" >
    <div class="container post-grid">
        <div class="py-4">
            <?php get_template_part("blocks/WYSIWYG"); ?>
        </div>
        <div class="row justify-content-between mb-3">
            <div class="col-lg-6 col-12 d-flex flex-row ">
            <div class="btn-group " role="group" aria-label="Basic example">
                <button class="btn btn-light border border-muted  text-decoration-none" type="button" data-mixitup-control data-sort="added:desc" >
                    <i class="far fa-calendar"></i>
                    New
                </button>
                <button class="btn btn-light border border-muted  text-decoration-none" type="button" data-mixitup-control data-sort="rate:desc" >
                    <i class="far fa-star"></i>
                    Top
                </button>
            </div>
            </div>
            <div class="col-lg-6 col-12 d-flex flex-row justify-content-end">
                <div class="btn-group btn-group-toggle border border-muted " data-toggle="buttons">
                    <label class="btn btn-light" data-mixitup-control data-filter="all">
                        <input type="radio" name="options" id="option1" autocomplete="off" checked> All
                    </label>
                    <?php foreach($terms as $term): ?>
                        <label class="btn btn-light " data-mixitup-control data-filter=".<?php echo $term->slug ?>">
                            <input type="radio" name="options" id="option1" autocomplete="off" checked> <?php echo $term->name; ?>
                        </label>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
       
        <?php
            set_query_var( 'unique', $unique_id );
            get_template_part("blocks/item-selector"); 
        ?>
    </div>
</div>


<script>
(function ($) {
    $('document').ready(function () {
        var mixupElement = $('#cont-<?php echo $unique_id; ?>');
        var mixer = mixitup(mixupElement, {
            selectors: {
                control: '[data-mixitup-control]'
            },
            animation: {
                duration: 300 ,
                easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)'
            }
        });

    })
})(jQuery);

</script>