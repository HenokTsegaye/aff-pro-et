<?php
/**
 * Expected Variables
 * $selected_posts
 */

?>
<?php if($selected_posts): ?>
    <div class="row">
        <?php foreach($selected_posts as $post): setup_postdata( $post); ?>
            <?php get_template_part('blocks/components/post', 'card'); ?>
        <?php endforeach; wp_reset_postdata(); ?>
    </div>
<?php endif; ?>