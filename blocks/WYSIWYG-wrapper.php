<?php

/**
 * Expected Variables
 * $title
 * $content
 * $alignment
 * $background_color
 * $text_color
 */

 $background_color = get_query_var("background_color", false);
 $text_color = get_query_var("text_color", false);
?>
 <div style="<?php echo $background_color?"background-color:".$background_color. ";":null; echo $text_color?"color:".$text_color. ";":null; ?>" >
    <div class="wysiwyg-wrapper container py-4"  >
        <?php get_template_part("blocks/WYSIWYG"); ?>
    </div>
 </div>