<?php

/**
 * Expected variables
 * $title
 * $content
 * $alignment
 */
$alignment = get_query_var('alignment', 'left');
?>
<div class=<?php printf("text-%s", $alignment); ?> >
   <?php if (isset($title)): ?>
    <h2 class="mb-4 text-capitalize" > <?php echo $title; ?> </h2>
   <?php endif;?>

   <?php if (isset($content)): ?>
   <div class="wysiwyg-content">
        <?php echo $content; ?>
   </div>
   <?php endif;?>
</div>