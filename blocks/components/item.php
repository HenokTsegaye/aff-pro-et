<?php
/*8
* renders the item as a list
* expected $post
*/
$local_timestamp = get_the_time('U');
$rating = get_field("rating")?get_field("rating"):0; 

$term_obj_list = get_the_terms( $post->ID, 'item_type' );
$terms_string = join(' ', wp_list_pluck($term_obj_list, 'slug'));



?>
<div class="mix <?php echo $terms_string; ?>" data-added="<?php echo $local_timestamp; ?>" data-rate="<?php echo $rating; ?>">
    <div class="col-md-12 col-12 px-1 mb-3">
    <div class="card shadow-sm border-0 d-flex flex-row rounded-2 overflow-hidden">
        <div class="col-4 px-0">

            <img src="<?php echo get_the_post_thumbnail_url($post ,'post-thumb'); ?>" alt="" class="card-img-top mt-0" style="max-height:250px;">
            <h4 class="card-title bg-white py-2 text-left card-body text-capitalize mb-0 ">
                    <?php the_title(); ?>
            <div class="card-text">
                <div class="star-rating-<?php the_id(); ?>" data-rating="<?php echo $rating; ?>"></div>
                <span class="text-muted text-vsm">
                <?php the_time( 'l, F jS, Y' ); ?>.

                </span>
            </div>
            </h4>
        </div>
        <div class="col-8   d-flex flex-column justify-content-around align-items-start">
            
            <div class="row justify-content-between">
                <div class="col-6">
                <ul class="list-group list-group-flush text-sm bg-transparent">

                    <?php while(have_rows('first_row')): 
                        the_row(); 
                        $key = get_sub_field('key');
                        $value = get_sub_field('value');
                        ?>
                        <li class="list-group-item border-0">
                            <?php echo $key ?>
                            <span class="float-right font-weight-bold" >
                                <?php echo $value; ?>
                            </span>
                        </li>
                    <?php endwhile; ?>
                    </ul>
                </div>
                <div class="col-6">
                <ul class="list-group list-group-flush text-sm bg-transparent">

                <?php while(have_rows('first_row')): 
                    the_row(); 
                    $key = get_sub_field('key');
                    $value = get_sub_field('value');
                    ?>
                    <li class="list-group-item border-0">
                        <?php echo $key ?>
                        <span class="float-right font-weight-bold" >
                            <?php echo $value; ?>
                        </span>
                    </li>
                <?php endwhile; ?>
                </ul>
                </div>
                
            </div>
            <div>
                <div class="w-100">
                <a class="btn btn-primary btn-sm my-3" href="<?php echo get_the_permalink( ); ?>" >
                    Read More
            </a>
                </div>
            <span class="badge badge-secondary px-2 py-1 rounded-2 mb-2">
                Historic
            </span>
        <div class="card-text text-muted text-vsm">
                <?php the_excerpt(); ?>
            </div>
            </div>
            
        </div>

    </div>
    <script>
    (function ($) {
        $(document).ready(function () {
            $(".star-rating-<?php the_ID(); ?>").starRating({
            totalStars: 5,
            starShape: 'rounded',
            starSize: 20,
            emptyColor: 'lightgray',
            hoverColor: 'salmon',
            activeColor: '#ffaa00',
        readOnly: true ,
            useGradient: false
            });
        })
    
    })(jQuery);
    
    </script>
</div>
</div>