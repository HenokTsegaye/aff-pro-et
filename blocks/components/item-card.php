<?php
/**
 * Renders items as a grid
 * expected $post
 * 
 */

$local_timestamp = get_the_time('U');
$rating = get_field("rating")?get_field("rating"):0; 

$term_obj_list = get_the_terms( $post->ID, 'item_type' );
$terms_string = join(' ', wp_list_pluck($term_obj_list, 'slug'));


?>
<div class="mix <?php echo $terms_string; ?>" data-added="<?php echo $local_timestamp; ?>" data-rate="<?php echo $rating; ?>">
    <div class="col-12 px-1 mb-3">
<div class="card border-0 shadow rounded-2 overflow-hidden w-100" >
    <?php the_post_thumbnail( 'medium', array('class'=> 'card-img-top')) ?>
    <div class="card-body">
        
        <div class="card-title text-uppercase text-sm"> <?php the_title(); ?></div>
        <div class="card-subtitle">
        <div class="star-rating-<?php the_id(); ?>" data-rating="<?php echo $rating; ?>"></div>

        </div>
    </div>
</div>

<script>
(function ($) {
    $(document).ready(function () {
        $(".star-rating-<?php the_ID(); ?>").starRating({
        totalStars: 5,
        starShape: 'rounded',
        starSize: 20,
        emptyColor: 'lightgray',
        hoverColor: 'salmon',
        activeColor: '#ffaa00',
    readOnly: true ,
        useGradient: false
        });
    })

})(jQuery);

</script>

    </div>
</div>