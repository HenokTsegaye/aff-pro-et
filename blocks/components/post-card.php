<?php

/**
 * Post is expected
 */
global $post;
?>
<div class="col-md-4 col-12 px-1 mb-3">
    <div class="card rounded-1 overflow-hidden border-0">
        
        <img src="<?php echo get_the_post_thumbnail_url($post ,'post-thumb'); ?>" alt="" class="card-img-top" style="max-height:280px;">
        <h4 class="card-title bg-light py-2 text-left card-body text-capitalize">
                <?php the_title(); ?>
        <div class="card-text">
            <span class="text-muted text-vsm">
            <?php the_time( 'l, F jS, Y' ); ?>.

            </span>
            </div>
        </h4>
        
        <div class="card-body d-flex flex-column justify-content-around py-0" style="height:120px;" >
            
            <div class="card-text text-sm">
                <?php the_excerpt(); ?>
            </div>
            
        </div>
    </div>
</div>