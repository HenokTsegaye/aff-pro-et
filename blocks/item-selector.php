<?php
/**
 * Expected Variables
 * $selected_items
 * $unique
 * $list_type
 */
$list_type = get_query_var( 'list_type', 'list' );
?>
<?php if($selected_items): ?>
    <div id="cont-<?php echo $unique; ?>" class="row">
        <?php foreach($selected_items as $post): 
            setup_postdata( $post);
            if($list_type == 'grid') {

                get_template_part( "blocks/components/item-card" );
            } else {
                get_template_part( "blocks/components/item" );

            }
            ?>

        <?php endforeach; wp_reset_postdata(); ?>
    </div>
<?php endif; ?>