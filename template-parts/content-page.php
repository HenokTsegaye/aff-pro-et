<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Affiliate_Program_ET
 */

global $post;

?>

<article id="post-<?php the_ID();?>" <?php post_class();?>>
	<?php
if (have_rows("blocks")):
    while (have_rows("blocks")): the_row();
        if (get_row_layout() == 'normal_wysiwyg'):
            $title = get_sub_field("title");
            $content = get_sub_field("content");
            $background = get_sub_field("background");
            $alignment = get_sub_field("alignment");

            set_query_var('title', $title);
            set_query_var('content', $content);
            set_query_var('alignment', $alignment);

            if (!$background['use_default_background_color']) {
                set_query_var('background_color', $background['background_color']);
            }

            if (!$background['use_default_text_color']) {
                set_query_var('text_color', $background['text_color']);
            }

            /**
             * invite in the template part
             */
			get_template_part('blocks/WYSIWYG', 'wrapper');
		elseif(get_row_layout() == 'post_grid'):

			$title = get_sub_field("title");
			$content = get_sub_field("content_text");
			$selected_posts = get_sub_field("select_posts");
			$appearance_group = get_sub_field("appearance_group");

			if($appearance_group['list_type']) {
				set_query_var( 'list_type', $appearance_group['list_type'] );
			}

			if(!$appearance_group['use_default_background']) {
				set_query_var( 'background_color', $appearance_group['background_color'] );
			}
			
			if(!$appearance_group['use_default_text_color']) {
				set_query_var( 'text_color', $appearance_group['text_color'] );
			}

			set_query_var( 'alignment', $appearance_group['text_alignment']);
			set_query_var( 'title', $title );
			set_query_var( 'content', $content );
			set_query_var( 'selected_posts', $selected_posts );

			get_template_part( 'blocks/post-grid', 'wrapper' );

		elseif(get_row_layout() == 'item_listing'):
			$title = get_sub_field('item_listing_title');
			$content = get_sub_field('item_listing_content');
			$listing_type = get_sub_field('listing_type');
			$select_items = get_sub_field('select_items');
			$appearance_group = get_sub_field('appearance_group');
			
			if(!$appearance_group['use_default_background_color']) {
				set_query_var( 'background_color', $appearance_group['background_color'] );
			}


			if(!$appearance_group['alignment']) {
				set_query_var( 'alignment', $appearance_group['alignment'] );
			}
			set_query_var( 'title', $title );
			set_query_var( 'content', $content );
			set_query_var( 'list_type', $listing_type );
			set_query_var( 'selected_items', $select_items );

			get_template_part("blocks/item", "wrapper");
		elseif(get_row_layout() === 'header_page'):
			$content = get_sub_field('content');
			$first_button = get_sub_field('first_button');	
			$secondary_button = get_sub_field('secondary_button');

			set_query_var( 'content', $content );
			set_query_var( 'first_button', $first_button );
			set_query_var( 'second_button', $secondary_button );

			get_template_part( 'blocks/content', 'wrapper' );
        endif;
    endwhile;
endif;

?>


	<?php if (get_edit_post_link()): ?>
		<footer class="entry-footer bg-light text-center">
			<?php
edit_post_link(
    sprintf(
        wp_kses(
            /* translators: %s: Name of current post. Only visible to screen readers */
            __('Edit <span class="screen-reader-text">%s</span>', 'affiliate-program-et'),
            array(
                'span' => array(
                    'class' => array(),
                ),
            )
        ),
        wp_kses_post(get_the_title())
    ),
    '<span class="edit-link">',
    '</span>'
);
?>
		</footer><!-- .entry-footer -->
	<?php endif;?>
</article><!-- #post-<?php the_ID();?> -->
