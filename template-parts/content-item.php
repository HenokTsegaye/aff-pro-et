<?php

global $post;
if(!wp_script_is("star-rating-js")) {
    wp_enqueue_script( 'start-rating-js');
    wp_enqueue_script( 'mixitup');
}
$rating = get_field("rating")?get_field("rating"):0; 

?>
<div class="position-relative bg-light "  >
<div style="background:rgba(0,0,0,0);" class="py-5">
<div class="container">
<div class="row">
    <div class="col-12 d-flex flex-column align-items-start">

    <h1 class="text-dark text-capitalize text-left">  <?php the_title(); ?>  </h1>
<div class="star-rating-<?php the_id(); ?>" data-rating="<?php echo $rating; ?>"></div>
    </div>
</div>

</div>
</div>

</div>


<div class="container mt-4">
    <div class="row">
        <div class="col-12">
            <?php the_content(); ?>
        </div>
    </div>
</div>
<script>
(function ($) {
    $(document).ready(function () {
        $(".star-rating-<?php the_ID(); ?>").starRating({
        totalStars: 5,
        starShape: 'rounded',
        starSize: 20,
        emptyColor: 'lightgray',
        hoverColor: 'salmon',
        activeColor: '#ffaa00',
    readOnly: true ,
        useGradient: false
        });
    })

})(jQuery);

</script>