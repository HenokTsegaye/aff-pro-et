<?php

/**
 * Register a custom post type called "item".
 *
 * @see get_post_type_labels() for label keys.
 */


function wpdocs_codex_item_init() {
    $text_domain = 'affiliate-program-et';
    $labels = array(
        'name'                  => _x( 'items', 'Post type general name', $text_domain ),
        'singular_name'         => _x( 'item', 'Post type singular name', $text_domain ),
        'menu_name'             => _x( 'items', 'Admin Menu text', $text_domain ),
        'name_admin_bar'        => _x( 'item', 'Add New on Toolbar', $text_domain ),
        'add_new'               => __( 'Add New', $text_domain ),
        'add_new_item'          => __( 'Add New item', $text_domain ),
        'new_item'              => __( 'New item', $text_domain ),
        'edit_item'             => __( 'Edit item', $text_domain ),
        'view_item'             => __( 'View item', $text_domain ),
        'all_items'             => __( 'All items', $text_domain ),
        'search_items'          => __( 'Search items', $text_domain ),
        'parent_item_colon'     => __( 'Parent items:', $text_domain ),
        'not_found'             => __( 'No items found.', $text_domain ),
        'not_found_in_trash'    => __( 'No items found in Trash.', $text_domain ),
        'featured_image'        => _x( 'item Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', $text_domain ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', $text_domain ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', $text_domain ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', $text_domain ),
        'archives'              => _x( 'item archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', $text_domain ),
        'insert_into_item'      => _x( 'Insert into item', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', $text_domain ),
        'uploaded_to_this_item' => _x( 'Uploaded to this item', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', $text_domain ),
        'filter_items_list'     => _x( 'Filter items list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', $text_domain ),
        'items_list_navigation' => _x( 'items list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', $text_domain ),
        'items_list'            => _x( 'items list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', $text_domain ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'item' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
    );
 
    register_post_type( 'item', $args );
}
 
add_action( 'init', 'wpdocs_codex_item_init' );