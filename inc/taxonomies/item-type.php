<?php
/**
 * Item type taxonomy
 */

$labels = array(
    'name'              => _x( 'Item Types', 'Item Types', 'affiliate-program-et' ),
    'singular_name'     => _x( 'Item Type', 'Item Type', 'affiliate-program-et' ),
    'search_items'      => __( 'Search Item Types', 'affiliate-program-et' ),
    'all_items'         => __( 'All Item Types', 'affiliate-program-et' ),
    'parent_item'       => __( 'Parent Item Type', 'affiliate-program-et' ),
    'parent_item_colon' => __( 'Parent Item Type:', 'affiliate-program-et' ),
    'edit_item'         => __( 'Edit Item Type', 'affiliate-program-et' ),
    'update_item'       => __( 'Update Item Type', 'affiliate-program-et' ),
    'add_new_item'      => __( 'Add New Item Type', 'affiliate-program-et' ),
    'new_item_name'     => __( 'New Item Type Name', 'affiliate-program-et' ),
    'menu_name'         => __( 'Item Type', 'affiliate-program-et' ),
);

$args = array(
    'hierarchical'      => true,
    'labels'            => $labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'item_type' ),
);

register_taxonomy( 'item_type', array( 'item' ), $args );