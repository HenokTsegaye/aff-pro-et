<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Affiliate_Program_ET
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header jumbotron bg-light">
				<div class="container">
					<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description mb-4">', '</div>' );
					?>

				</div>
			</header><!-- .page-header -->

			<div class="container">
				<div class="row">
				<?php
				while ( have_posts() ) :
					the_post();

					/*
					* Include the Post-Type-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Type name) and that will be used instead.
					*/
				
					if(get_post_type() == 'post') {

						get_template_part( 'blocks/components/post', 'card' );
					} else if(get_post_type() == 'item') {

						get_template_part( 'blocks/components/item', 'card' );
					}

				endwhile;
				?>
				</div>
			<?php
			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</div>
	</main><!-- #main -->

<?php
get_footer();
